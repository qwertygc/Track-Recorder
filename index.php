<?php
define('LAYER', 'http://{s}.tile.osm.org/{z}/{x}/{y}.png');
$databasefile = isset($_GET['db']) ? $_GET['db'] : 'default';
if(!file_exists($databasefile.'.db')) {
	try {
			$bdd = new PDO('sqlite:'.$databasefile.'.db');
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
			catch (PDOException $e) {
			echo "<p>Error : " . $e->getMessage() . "</p>";
			exit();
		}
		$bdd->query("CREATE TABLE tracks (
		id INTEGER PRIMARY KEY,
		lat TEXT,
		lon TEXT,
		timestamp int(20),
		hdop TEXT,
		altitude TEXT,
		speed TEXT,
		bearing TEXT);");

}
else {
	try {
			$bdd = new PDO('sqlite:'.$databasefile.'.db');
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
			catch (PDOException $e) {
			echo "<p>Error : " . $e->getMessage() . "</p>";
			exit();
		}
}
if(isset($_GET['lat'])) {
			$db = $bdd->prepare("INSERT INTO tracks (lat, lon, timestamp, hdop, altitude, speed, bearing) VALUES (?,?,?,?,?,?,?);");
			$db->execute(array($_GET['lat'], $_GET['lon'], $_GET['timestamp'], $_GET['hdop'], $_GET['altitude'], $_GET['speed'], $_GET['bearing']));

}
?>
<!doctype html>
<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
    <link rel="stylesheet" href="https://framagit.org/champlywood/Reset.css/raw/master/reset.css" />
 	<meta http-equiv="refresh" content="15;"> 
	<meta charset="utf-8"/>
	<style>
	body,
	html {
		margin: 0;
		padding: 0;
		font-family:sans-serif;
		word-wrap: break-word;
		font-size: 1em;
	}
	table {
		width:100%;
	}
	td {
		border-right:1px solid #333;
		text-align:center;
	}
	#macarte {
		margin:auto;
		width:100%;
	}
	footer {
		text-align:center;
		background:#999;
		color: #fff;
		height:5vh;
	}
	footer a {
		color:#fff;
		text-decoration:none;
	}
	</style>
	</head>
	<body>
<?php
if(isset($_GET['a'])) {
	switch($_GET['a']) {
		case 'table':
			echo '<h1>Raw Table</h1><table>
			<tr>
				<th>ID</th>
				<th>Lat</th>
				<th>Lon</th>
				<th>Timestamp</th>
				<th>date</th>
				<th>Hdop</th>
				<th>altitude</th>
				<th>speed</th>
				<th>bearing</th>
				<th>Share</th>
			</tr>';
		$something = $bdd->query('SELECT * FROM tracks');
		$something->setFetchMode(PDO::FETCH_BOTH);
		while($data = $something->fetch()) {
		echo '<tr>
				<td>'.$data['id'].'</td>
				<td>'.$data['lat'].'</td>
				<td>'.$data['lon'].'</td>
				<td>'.$data['timestamp'].'</td>
				<td>'.date('c', $data['timestamp']/1000).'</td>
				<td>'.$data['hdop'].'</td>
				<td>'.$data['altitude'].'</td>
				<td>'.$data['speed'].'</td>
				<td>'.$data['bearing'].'</td>
				<td><a href="geo:'.$data['lat'].','.$data['lon'].'">🔗</a></td>
			</tr>';
		}
		echo '</table>';
		break;
		case 'archive':
			echo '<h1>Archive</h1><form method="post"><input type="submit" name="archive" value="Archive the database"/></form>';
				if(isset($_POST['archive'])) {
					rename($_GET['db'].'.db', ''.date('YmdHis', time()).'-'.$_GET['db'].'.db');
				}
			echo '<ul>';
			$dir= array_diff(scandir('./'), array('..', '.', 'index.php'));
			foreach($dir as $file) {
				echo '<li><a href="?db='.basename($file, '.db').'">use this version : '.$file.'</a></li>';
			}
			echo '</ul>';
			echo '<p>URL for the tracking (change « db » name in url for have many tracking) : </p><input type="text" value="http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'?lat={0}&lon={1}&timestamp={2}&hdop={3}&altitude={4}&speed={5}&db='.$databasefile.'" style="width:100%"/>';
		break;
	}
}
else {
?>
<div id="macarte" style="width:100%; height:95vh;"></div>
<script>
<?php $something = $bdd->query('SELECT * FROM tracks ORDER BY id DESC'); $data = $something->fetch(); ?>
var carte = L.map('macarte').setView([<?php echo $data['lat']; ?>, <?php echo $data['lon']; ?>], 9);
L.tileLayer("<?php echo LAYER; ?>", {
attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(carte);
var track = L.polyline([
<?php
$something = $bdd->query('SELECT * FROM tracks');
$something->setFetchMode(PDO::FETCH_BOTH);
while($data = $something->fetch()) {
	echo '['.$data['lat'].', '.$data['lon'].'],'.PHP_EOL;
}
?>
], {color: 'red'}).addTo(carte);
<?php $something = $bdd->query('SELECT * FROM tracks ORDER BY id DESC'); $data = $something->fetch(); ?>
var marker = L.marker([<?php echo $data['lat']; ?>, <?php echo $data['lon']; ?>]).addTo(carte);
</script>
<?php
}
?>

<footer><a href="index.php?db=<?php echo $databasefile; ?>">Map</a> | <a href="?a=table&db=<?php echo $databasefile; ?>">Raw table</a> | <a href="?a=archive&db=<?php echo $databasefile; ?>">Archive</a> | <a href="https://framagit.org/qwertygc/Track-Recorder/">Server</a> | <a href="http://osmand.net/features?id=trip-recording-plugin">Plugin</a></footer>
</body>
</html>
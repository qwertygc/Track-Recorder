<?php
$root = 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'?a=server';
?>
<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
	<style>
	body,
	html {
		margin: 0;
		padding: 0;
		font-family:sans-serif;
		word-wrap: break-word;
		font-size: 1em;
	}
	table {
		width:100%;
	}
	td {
		border-right:1px solid #333;
		text-align:center;
	}
	#macarte {
		margin:auto;
		width:100%;
	}
	footer {
		text-align:center;
		background:#999;
		color: #fff;
		height:5vh;
	}
	footer a {
		color:#fff;
		text-decoration:none;
	}
	input[type="button"] {
		padding:1em;
		min-width:50px;
		margin:auto;
		font-size:1.5em;
		cursor: pointer;
		color: #fff;
		box-sizing: content-box;
		border-radius: 5px;
		box-shadow: 0 0 20px 0 rgba(0,0,0,.3);
	}
	</style>
	<script>
	function surveillePosition(position) {
		var infopos = "Position déterminée :\n";
		infopos += "Latitude : "+position.coords.latitude +"\n";
		infopos += "Longitude: "+position.coords.longitude+"\n";
		infopos += "Altitude : "+position.coords.altitude +"\n";
		infopos += "Vitesse  : "+position.coords.speed +"\n";
		document.getElementById("infoposition").innerHTML = infopos;
		xhr = new XMLHttpRequest();
		xhr.open('POST', '<?php echo $root; ?>', true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.send(encodeURI('lat=' + position.coords.latitude + '&lon=' + position.coords.longitude + '&timestamp=' + Date.now() + '&altitude=' + position.coords.altitude + '&speed=' + position.coords.speed));
	}

	function start() {
		if(navigator.geolocation) {
			survId = navigator.geolocation.watchPosition(surveillePosition);
		} else {
			alert("Ce navigateur ne supporte pas la géolocalisation");
		}
	}
	function stop() {
		navigator.geolocation.clearWatch(survId);
		document.getElementById("infoposition").innerHTML = '';
	}
	</script>
</head>
<body>
<?php
if(!file_exists('data.json')) {
	file_put_contents('data.json', json_encode(array()));
}
if(isset($_GET['a'])) {
	switch($_GET['a']) {
		case 'client':
			echo '	<input type="button" onclick="start();" value="Start" style="background-color:green;">
					<input type="button" onclick="stop();" value="Stop"  style="background-color:#c0392b;">
					<div id="infoposition"></div>';
		break;
		case 'server':
			$data = json_decode(file_get_contents('data.json'), true);
			$data[] = $_REQUEST;
			file_put_contents('data.json', json_encode($data));
		break;
		case 'table':
			echo '<h1>Raw Table</h1><table>
			<tr>
				<th>Lat</th>
				<th>Lon</th>
				<th>Timestamp</th>
				<th>date</th>
				<th>altitude</th>
				<th>speed</th>
				<th>Share</th>
			</tr>';
		$datas = json_decode(file_get_contents('data.json'), true);
		foreach($datas as $data) {
		echo '<tr>
				<td>'.$data['lat'].'</td>
				<td>'.$data['lon'].'</td>
				<td>'.$data['timestamp'].'</td>
				<td>'.date('c', $data['timestamp']/1000).'</td>
				<td>'.$data['altitude'].'</td>
				<td>'.$data['speed'].'</td>
				<td><a href="geo:'.$data['lat'].','.$data['lon'].'">🔗</a></td>
			</tr>';
		}
		echo '</table>';
		break;
	}
}
else {
$data = json_decode(file_get_contents('data.json'), true);
echo '<div id="macarte" style="width:100%; height:95vh;"></div>';
?>
<script>
var carte = L.map('macarte').setView([<?php echo $data[0]['lat']; ?>, <?php echo $data[0]['lon']; ?>], 9);
L.tileLayer("https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png", {
attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(carte);
var track = L.polyline([
<?php
foreach($data as $d) {
	echo '['.$d['lat'].', '.$d['lon'].'],'.PHP_EOL;
}
?>
], {color: 'red'}).addTo(carte);
</script>
<?php }
?>
<footer>
	<a href="?">Map</a> | <a href="?a=client">Collect Data</a> | <a href="?a=table">Raw Table</a> | <a href="https://framagit.org/qwertygc/Track-Recorder">Source code</a>
</footer>
</body>
</html>
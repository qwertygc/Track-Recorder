Partie serveur pour le « Track Recorder » (plugin d'OSMAnd, « Enregistrement d'itinéraire » en français). Le plugin Android n'est pas de moi, j'ai juste codé le serveur en PHP.

# Installation
Côté serveur : mettre index.php sur un serveur web (il faudra Sqlite).

Côté Client : installer OSMAnd. Installer le plugin « Enregistrement d'itinéraire ».

Dans les paramètres :
- Adresse web pour le suivie en ligne : mettre votre serveur ;
- Cocher : Suivi en ligne ;

[Présentation](https://zestedesavoir.com/billets/2700/une-alternative-au-partage-de-position-de-google-maps/)

index2.php : la collecte des données se fait via le navigateur. La base de données est un fichier JSON et il y a moins d'options de configuration.